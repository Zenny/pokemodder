use num_derive::FromPrimitive;

#[repr(u8)]
#[derive(Copy, Clone, PartialOrd, PartialEq, Debug, FromPrimitive)]
pub enum Growth {
    MediumFast = 0,
    Erratic = 1,
    Fluctuating = 2,
    MediumSlow = 3,
    Fast = 4,
    Slow = 5,
}
