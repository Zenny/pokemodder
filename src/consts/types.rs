use num_derive::FromPrimitive;

#[repr(u8)]
#[derive(Copy, Clone, PartialOrd, PartialEq, Debug, FromPrimitive)]
pub enum Type {
    Normal = 0,
    Fighting = 1,
    Flying = 2,
    Poison = 3,
    Ground = 4,
    Rock = 5,
    Bug = 6,
    Ghost = 7,
    Steel = 8,
    Mystery = 9,
    Fire = 10,
    Water = 11,
    Grass = 12,
    Electric = 13,
    Psychic = 14,
    Ice = 15,
    Dragon = 16,
    Dark = 17,
}
