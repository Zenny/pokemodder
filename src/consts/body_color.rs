use num_derive::FromPrimitive;

#[repr(u8)]
#[derive(Copy, Clone, PartialOrd, PartialEq, Debug, FromPrimitive)]
pub enum BodyColor {
    Red = 0,
    Blue = 1,
    Yellow = 2,
    Green = 3,
    Black = 4,
    Brown = 5,
    Purple = 6,
    Gray = 7,
    White = 8,
    Pink = 9,
}
