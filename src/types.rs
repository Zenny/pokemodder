use crate::*;
use bytes::{Buf, BufMut};
use num_traits::FromPrimitive;
use std::fmt::Debug;

pub const NUM_TRAINERS: u16 = 743;
pub const GENDER_F: u8 = 1 << 7;

#[derive(Clone, PartialOrd, PartialEq, Debug)]
pub struct Addressed<T: Clone + PartialOrd + PartialEq + Debug> {
    pub addr: usize,
    pub inner: T,
}

impl<T: Clone + PartialOrd + PartialEq + Debug> Addressed<T> {
    pub fn new(addr: usize, inner: T) -> Self {
        Self { addr, inner }
    }

    /// Grabs the inner data
    pub fn unwrap(self) -> (usize, T) {
        (self.addr, self.inner)
    }
}

impl<'a> TryFrom<Addressed<&'a [u8]>> for Addressed<BaseStats> {
    type Error = ();

    fn try_from(value: Addressed<&'a [u8]>) -> Result<Self, Self::Error> {
        Ok(Self {
            addr: value.addr,
            inner: value.inner.try_into()?,
        })
    }
}

/*
.baseHP = 45,
.baseAttack = 49,
.baseDefense = 49,
.baseSpeed = 45,
.baseSpAttack = 65,
.baseSpDefense = 65,
.type1 = TYPE_GRASS,
.type2 = TYPE_POISON,
.catchRate = 45,
.expYield = 64,
.evYield_HP = 0, .evYield_Attack = 0, .evYield_Defense = 0, .evYield_Speed = 0,
.evYield_SpAttack = 1, .evYield_SpDefense = 0,
.item1 = ITEM_NONE,
.item2 = ITEM_NONE,
.genderRatio = PERCENT_FEMALE(12.5),
.eggCycles = 20,
.friendship = 70,
.growthRate = GROWTH_MEDIUM_SLOW,
.eggGroup1 = EGG_GROUP_MONSTER,
.eggGroup2 = EGG_GROUP_GRASS,
.abilities = { ABILITY_OVERGROW, ABILITY_NONE },
.safariZoneFleeRate = 0,
.bodyColor = BODY_COLOR_GREEN,
.noFlip = FALSE,

struct BaseStats
{
 /* 0x00 */ u8 baseHP;
 /* 0x01 */ u8 baseAttack;
 /* 0x02 */ u8 baseDefense;
 /* 0x03 */ u8 baseSpeed;
 /* 0x04 */ u8 baseSpAttack;
 /* 0x05 */ u8 baseSpDefense;
 /* 0x06 */ u8 type1;
 /* 0x07 */ u8 type2;
 /* 0x08 */ u8 catchRate;
 /* 0x09 */ u8 expYield;
 /* 0x0A */ u16 evYield_HP:2;
 /* 0x0A */ u16 evYield_Attack:2;
 /* 0x0A */ u16 evYield_Defense:2;
 /* 0x0A */ u16 evYield_Speed:2;
 /* 0x0B */ u16 evYield_SpAttack:2;
 /* 0x0B */ u16 evYield_SpDefense:2;
 /* 0x0C */ u16 item1;
 /* 0x0E */ u16 item2;
 /* 0x10 */ u8 genderRatio;
 /* 0x11 */ u8 eggCycles;
 /* 0x12 */ u8 friendship;
 /* 0x13 */ u8 growthRate;
 /* 0x14 */ u8 eggGroup1;
 /* 0x15 */ u8 eggGroup2;
 /* 0x16 */ u8 abilities[2];
 /* 0x18 */ u8 safariZoneFleeRate;
 /* 0x19 */ u8 bodyColor : 7;
            u8 noFlip : 1;
};
*/
#[derive(Clone, PartialOrd, PartialEq, Debug)]
pub struct BaseStats {
    pub base_hp: u8,
    pub base_atk: u8,
    pub base_def: u8,
    pub base_spd: u8,
    pub base_spatk: u8,
    pub base_spdef: u8,
    pub type1: Type,
    pub type2: Type,
    pub catch_rate: u8,
    pub exp_yield: u8,

    /*
        These are split out from u16s
        and are 2 bits each
    */
    pub ev_yield_hp: u8,
    pub ev_yield_atk: u8,
    pub ev_yield_def: u8,
    pub ev_yield_spd: u8,
    pub ev_yield_spatk: u8,
    pub ev_yield_spdef: u8,
    pub item1: Item,
    pub item2: Item,
    pub gender_ratio: u8,
    pub egg_cycles: u8,
    pub friendship: u8,
    pub growth_rate: Growth,
    pub egg_group1: EggGroup,
    pub egg_group2: EggGroup,
    pub ability1: Ability,
    pub ability2: Ability,
    pub safari_zone_flee_rate: u8,
    pub body_color: BodyColor,
    pub no_flip: bool,
}

impl BaseStats {
    pub fn to_bytes(&self) -> [u8; 26] {
        let item1 = (self.item1 as u16).to_le_bytes();
        let item2 = (self.item1 as u16).to_le_bytes();
        [
            self.base_hp,
            self.base_atk,
            self.base_def,
            self.base_spd,
            self.base_spatk,
            self.base_spdef,
            self.type1 as u8,
            self.type2 as u8,
            self.catch_rate,
            self.exp_yield,
            self.ev_yield_hp
                | (self.ev_yield_atk << 2)
                | (self.ev_yield_def << 4)
                | (self.ev_yield_spd << 6),
            self.ev_yield_spatk | (self.ev_yield_spdef << 2),
            item1[0],
            item1[1],
            item2[0],
            item2[1],
            self.gender_ratio,
            self.egg_cycles,
            self.friendship,
            self.growth_rate as u8,
            self.egg_group1 as u8,
            self.egg_group2 as u8,
            self.ability1 as u8,
            self.ability2 as u8,
            self.safari_zone_flee_rate,
            self.body_color as u8 | (if self.no_flip { 0x80 } else { 0 }),
        ]
    }
}

impl TryFrom<&[u8]> for BaseStats {
    type Error = ();

    fn try_from(value: &[u8]) -> Result<Self, Self::Error> {
        // Seems there's a 2 byte buffer between each entry,
        // it should be 1C but we can work with 1A
        if value.len() < 0x1A {
            return Err(());
        }

        Ok(Self {
            base_hp: value[0],
            base_atk: value[1],
            base_def: value[2],
            base_spd: value[3],
            base_spatk: value[4],
            base_spdef: value[5],
            type1: Type::from_u8(value[6]).ok_or(())?,
            type2: Type::from_u8(value[7]).ok_or(())?,
            catch_rate: value[8],
            exp_yield: value[9],
            ev_yield_hp: value[10] & 0b0000_0011,
            ev_yield_atk: (value[10] & 0b0000_1100) >> 2,
            ev_yield_def: (value[10] & 0b0011_0000) >> 4,
            ev_yield_spd: (value[10] & 0b1100_0000) >> 6,
            ev_yield_spatk: value[11] & 0b0000_0011,
            ev_yield_spdef: (value[11] & 0b0000_1100) >> 2,
            item1: Item::from_u16((&value[12..14]).get_u16_le()).ok_or(())?,
            item2: Item::from_u16((&value[14..16]).get_u16_le()).ok_or(())?,
            gender_ratio: value[16],
            egg_cycles: value[17],
            friendship: value[18],
            growth_rate: Growth::from_u8(value[19]).ok_or(())?,
            egg_group1: EggGroup::from_u8(value[20]).ok_or(())?,
            egg_group2: EggGroup::from_u8(value[21]).ok_or(())?,
            ability1: Ability::from_u8(value[22]).ok_or(())?,
            ability2: Ability::from_u8(value[23]).ok_or(())?,
            safari_zone_flee_rate: value[24],
            body_color: BodyColor::from_u8(value[25] & 0x7F).ok_or(())?,
            no_flip: value[25] & 0x80 == 0x80,
        })
    }
}

/// Moves are masked to 9 bits.
#[derive(Clone, PartialOrd, PartialEq, Debug)]
pub struct LearnSet {
    pub level: u8,
    pub mv: Move,
}

impl LearnSet {
    pub fn to_bytes(&self) -> [u8; 2] {
        let mut pack: u16 = (self.level as u16) << 9;
        pack |= self.mv as u16 & 0b0000_0001_1111_1111;

        pack.to_le_bytes()
    }
}

impl TryFrom<u16> for LearnSet {
    type Error = ();

    fn try_from(value: u16) -> Result<Self, Self::Error> {
        let mv = value & 0b0000_0001_1111_1111;
        let level = value >> 9;

        Ok(LearnSet {
            level: level as u8,
            mv: Move::from_u16(mv).ok_or(())?,
        })
    }
}

#[derive(Copy, Clone, Debug, PartialOrd, PartialEq)]
pub enum Gender {
    M,
    F,
}

impl Gender {
    pub fn to_flag(&self) -> u8 {
        if let Self::F = self {
            GENDER_F
        } else {
            0
        }
    }
}

impl From<u8> for Gender {
    fn from(value: u8) -> Self {
        match value & GENDER_F {
            0 => Self::M,
            _ => Self::F,
        }
    }
}

// 00 00
// 05
// 00 // ?? padding
// 17 00
// 00 00 // item

// 00 00
// 0E
// 00 // ?? padding
// 17 00
// 00 00 // item
// split mon --
// 00 00
// 0E
// 00 1B
// 00 00
// 00

// custitem
// 00 00
// 0E
// 00 // ?? padding
// 17 00
// 01 00 // item
// split --
// 00 00
// 0E
// 00
// 1B 00
// 02 00

// custmoves
// 00 00
// 01
// 00 // ?? padding
// 17 00
// 00 00 // item
// 01 00
// 02 00
// 00 00
// 00 00 // moves
// split --
// 00 00
// 01
// 00
// 1B 00
// 00 00 // item
// 02 00
// 03 00
// 05 00
// 06 00
// Item bytes are always there even if unused
// all 4 move bytes added if custom move (per mon)
#[derive(Copy, Clone, PartialOrd, PartialEq, Debug)]
pub struct PartyMon {
    pub ev: u8, // technically a u16
    pub lvl: u8,
    pub species: Mon,
    pub held_item: Option<Item>,
    pub moves: Option<[Move; 4]>,
}

impl PartyMon {
    /// This implementation doesn't follow convention because it is not a static size.
    pub fn to_bytes(&self, custom_moves: bool) -> Vec<u8> {
        let mut ret = Vec::with_capacity(16);

        ret.push(self.ev);
        ret.push(0);
        ret.push(self.lvl);
        ret.push(0);
        ret.extend_from_slice(&(self.species as u16).to_le_bytes());
        ret.extend_from_slice(&(self.held_item.unwrap_or(Item::None) as u16).to_le_bytes());
        // All areas need to agree on if there's custom moves
        if custom_moves {
            if let Some(moves) = self.moves {
                for mv in moves {
                    ret.extend_from_slice(&(mv as u16).to_le_bytes());
                }
            }
        }

        ret
    }
}

impl PartyMon {
    pub fn try_from(value: &[u8], custom_moves: bool, custom_item: bool) -> Result<Self, ()> {
        if value.len() > 16 {
            return Err(());
        }

        Ok(Self {
            ev: value[0],
            lvl: value[2],
            species: Mon::from_u16((&value[4..6]).get_u16_le()).ok_or(())?,
            held_item: if custom_item {
                Some(Item::from_u16((&value[6..8]).get_u16_le()).ok_or(())?)
            } else {
                None
            },
            moves: if custom_moves {
                Some([
                    Move::from_u16((&value[8..10]).get_u16_le()).ok_or(())?,
                    Move::from_u16((&value[10..12]).get_u16_le()).ok_or(())?,
                    Move::from_u16((&value[12..14]).get_u16_le()).ok_or(())?,
                    Move::from_u16((&value[14..16]).get_u16_le()).ok_or(())?,
                ])
            } else {
                None
            },
        })
    }
}


/*
#define F_TRAINER_FEMALE (1 << 7)
#define F_TRAINER_PARTY_CUSTOM_MOVESET (1 << 0)
#define F_TRAINER_PARTY_HELD_ITEM      (1 << 1)
00 flags (01 = custom moves)
02 class
06 music // last bit is gender
00 pic
FF  00  00  00  00  00  00  00  00  00  00  00 name
00 00  00 00  00 00  00 00 items
00 00 00 00 double battle (01 00 00 00 = double)
01 00 00 00  ai flags
01 party size
00 00 00 ???
A0 A1 23 08 party ptr

00
60
08
83
C6  C3  C6  00  2D  00  C3  BB  C8  FF  00  00 (LIL & IAN)
00 00  00 00  00 00  00 00
01 00 00 00
01 00 00 00
02
00 00 00
60 CE 23 08
*/
#[derive(Clone, Debug, PartialOrd, PartialEq)]
pub struct Trainer {
    pub custom_moves: bool,
    pub custom_held_item: bool,
    pub class: u8,
    pub music: u8,
    pub gender: Gender, // last bit from music byte
    pub sprite: u8,
    pub name: MonString,
    pub items: [Item; 4],
    pub double_battle: bool,
    pub ai_flags: u8,
    pub(crate) party_size: u8,
    pub(crate) party: Addressed<[Option<PartyMon>; 6]>,
    pub(crate) prev_party_bytes: usize,
}

impl Trainer {
    pub fn get_party(&self) -> &[Option<PartyMon>; 6] {
        &self.party.inner
    }

    pub fn set_party(&mut self, party: [Option<PartyMon>; 6]) {
        self.party_size = party.iter().filter(|o| o.is_some()).count() as u8;
        self.party.inner = party;
    }

    pub fn party_to_bytes(&self) -> Vec<u8> {
        let mut party = Vec::with_capacity(16 * 6);

        for mon in &self.party.inner {
            if let Some(mon) = mon {
                party.extend_from_slice(mon.to_bytes(self.custom_moves).as_slice());
            }
        }

        party
    }

    pub fn to_bytes(&self) -> ([u8; 40], Addressed<Vec<u8>>) {
        let flags: u8 = if self.custom_moves { 1 } else { 0 } | if self.custom_held_item { 1 << 1 } else { 0 };
        let mut name_padded = self.name.bytes.clone();
        if !name_padded.ends_with(&[0xFF]) {
            name_padded.put_u8(0xFF);
        }
        name_padded.resize(12, 0);
        let mut main: [u8; 40] = [0; 40];
        let mut main_vec = vec![
            flags,
            self.class,
            self.music | self.gender.to_flag(),
            self.sprite,
        ];
        main_vec.extend_from_slice(&name_padded[..]);

        for item in self.items.iter() {
            main_vec.extend_from_slice(&(*item as u16).to_le_bytes());
        }

        main_vec.extend_from_slice(&[
            if self.double_battle { 1 } else { 0 }, 0, 0, 0,
            self.ai_flags, 0, 0, 0,
            self.party_size, 0, 0, 0,
        ]);
        // NOTE: Do this on setting the rom data if we relocate
        main_vec.extend_from_slice(&((self.party.addr + SYM_ADDR_TO_FILE) as u32).to_le_bytes());

        main.copy_from_slice(main_vec.as_slice());

        let party = self.party_to_bytes();

        (main, Addressed::new(self.party.addr, party))
    }
}

impl Trainer {
    /// The ROM is passed to resolve the party pointer
    pub fn try_from(value: &[u8], rom: &[u8]) -> Result<Self, ()> {
        if value.len() != 40 {
            return Err(());
        }

        let party_size = value[32];
        let party_addr = ((&value[36..40]).get_u32_le() as usize).saturating_sub(SYM_ADDR_TO_FILE);
        let mut party = [None; 6];

        let custom_moves = value[0] & 1 != 0;
        let custom_item = value[0] & (1 << 1) != 0;

        let mon_offset = if custom_moves {
            16
        } else {
            8
        };

        let prev_party_bytes = (mon_offset * party_size) as usize;

        if party_addr != 0 {
            for i in 0..party_size {
                let offset_addr = party_addr + (mon_offset * i) as usize;
                party[i as usize] = Some(PartyMon::try_from(&rom[offset_addr..offset_addr + mon_offset as usize], custom_moves, custom_item)?);
            }
        }

        let party = Addressed::new(party_addr, party);

        let mut items = [Item::None; 4];
        for i in 0..4 {
            items[i as usize] = Item::from_u16((&value[16 + (2 * i)..18 + (2 * i)]).get_u16_le()).ok_or(())?;
        }

        Ok(Self {
            custom_moves,
            custom_held_item: custom_item,
            class: value[1],
            music: value[2] & !(GENDER_F),
            gender: Gender::from(value[2]),
            sprite: value[3],
            name: MonString::new(&value[4..16]),
            items,
            double_battle: value[24] != 0,
            ai_flags: value[28],
            party_size,
            party,
            prev_party_bytes
        })
    }
}

/*
evo
04 00 // method
FF 00 // param
02 00 // target species
80 81 // padding?
---
04 00
FE 00
03 00
81 80
---
04 00
FD 00
04 00
80 81
---
04 00
FC 00
05 00
81 80
---
04 00
FB 00
06 00
80 81
 */

#[derive(Copy, Clone, Debug, Default, PartialOrd, PartialEq)]
pub struct Evolution {
    pub method: u8, // technically u16
    pub param: u16,
    pub species: Mon,
}

impl Evolution {
    pub fn to_bytes(&self) -> [u8; 8] {
        let method = (self.method as u16).to_le_bytes();
        let param = self.param.to_le_bytes();
        let species = (self.species as u16).to_le_bytes();
        [
            method[0],
            method[1],
            param[0],
            param[1],
            species[0],
            species[1],
            0, 0,
        ]
    }
}

impl TryFrom<&[u8]> for Evolution {
    type Error = ();

    fn try_from(value: &[u8]) -> Result<Self, Self::Error> {
        Ok(Self {
            method: value[0],
            param: (&value[2..4]).get_u16_le(),
            species: Mon::from_u16((&value[4..6]).get_u16_le()).ok_or(())?,
        })
    }
}