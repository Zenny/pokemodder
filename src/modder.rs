use crate::*;
use bytes::{Buf, BytesMut};
use std::collections::HashMap;
use std::fs::File;
use std::io;
use std::io::Read;
use std::path::Path;
use thiserror::Error;

/// Subtract by this to turn a sym address into a
/// file address. This only works if the sym address
/// is >= SYM_ADDR_TO_FILE which indicates that the
/// sym address is infact within the ROM file.
pub const SYM_ADDR_TO_FILE: usize = 0x08000000;

const SYM_FILES: [&str; 4] = [
    include_str!("syms/pokefirered.sym"),
    include_str!("syms/pokefirered_rev1.sym"),
    include_str!("syms/pokeleafgreen.sym"),
    include_str!("syms/pokeleafgreen_rev1.sym"),
];

pub struct SymData {
    pub addr: usize,
    pub len: usize,
}

#[repr(usize)]
#[derive(PartialEq, Copy, Clone, Debug)]
pub enum Version {
    FireV1 = 0,
    FireV1_1 = 1,
    LeafV1 = 2,
    LeafV1_1 = 3,
}

#[derive(Error, Debug)]
pub enum Error {
    #[error(transparent)]
    Io(#[from] io::Error),
    #[error("Specified path is a directory")]
    NotAFile,
    #[error("ROM does not appear valid")]
    InvalidRom,
    #[error("mon_id not valid")]
    InvalidId,
    #[error("Supplied data too large")]
    TooMuchData,
    #[error("Data does not appear valid")]
    InvalidData,
    #[error("No free space large enough")]
    NoFreeSpace,
    #[error("Invalid character supplied")]
    InvalidChar,
}

fn parse_sym(sym: &'static str) -> (HashMap<&'static str, SymData>, HashMap<usize, usize>) {
    let mut sym_map = HashMap::with_capacity(50844);
    let mut addr_size_map = HashMap::with_capacity(50844);

    for line in sym.split_terminator('\n').filter(|s| !s.is_empty()) {
        let mut fields = line.split_whitespace();
        let addr = usize::from_str_radix(fields.next().expect("no addr"), 16).expect("bad addr");
        let _kind = fields.next().expect("no kind"); // g = global  l = local?
        let len = usize::from_str_radix(fields.next().expect("no len"), 16).expect("bad len");

        sym_map.insert(fields.next().expect("no name"), SymData { addr, len });
        addr_size_map.insert(addr, len);
    }

    (sym_map, addr_size_map)
}

/// Addresses returned by these functions are based on FILE DATA ADDRESSES and
/// not exactly what the symbol files say.
pub struct Modder {
    pub data: BytesMut,
    pub version: Version,
    sym_map: HashMap<&'static str, SymData>,
    addr_size_map: HashMap<usize, usize>,
}

impl Modder {
    /// A version of None will autodetect the version and
    /// fail if the ROM seems invalid. Use a custom version
    /// only if your ROM's game code and/or version byte are
    /// changed from the originals and would fail.
    pub fn open<P: AsRef<Path>>(rom: P, version: Option<Version>) -> Result<Self, Error> {
        let mut buf = BytesMut::with_capacity(16_778_000);

        buf.resize(16_778_000, 0);

        let mut f = File::open(rom)?;
        let count = f.read(&mut buf)?;
        // 16mb
        if count < 16_777_216 {
            return Err(Error::InvalidRom);
        }

        buf.truncate(count);
        let version = if let Some(version) = version {
            version
        } else {
            // These are always the same for all supported rom types
            // but we "can't" read the sym to find out where to read
            // until we read this to know what sym to read.
            let game_code = &buf[0xAC..0xB2];
            let version_byte = &buf[0xBC];
            match game_code {
                b"BPRE01" => match version_byte {
                    0 => Version::FireV1,
                    1 => Version::FireV1_1,
                    _ => return Err(Error::InvalidRom),
                },
                b"BPGE01" => match version_byte {
                    0 => Version::LeafV1,
                    1 => Version::LeafV1_1,
                    _ => return Err(Error::InvalidRom),
                },
                _ => return Err(Error::InvalidRom),
            }
        };

        let (sym_map, addr_size_map) = parse_sym(SYM_FILES[version as usize]);

        Ok(Self {
            data: buf,
            sym_map,
            addr_size_map,
            version,
        })
    }

    pub fn save<P: AsRef<Path>>(&self, path: P) -> Result<(), Error> {
        if path.as_ref().is_dir() {
            return Err(Error::NotAFile);
        }

        std::fs::write(
            path,
            &self.data,
        )?;

        Ok(())
    }

    pub fn overwrite(&mut self, mut data: Addressed<&[u8]>) {
        if data.addr > SYM_ADDR_TO_FILE {
            data.addr -= SYM_ADDR_TO_FILE;
        }

        let old = &mut self.data[data.addr..data.addr + data.inner.len()];
        old.copy_from_slice(data.inner);
    }

    fn find_free_memory(&self, size: usize) -> Option<usize> {
        let mut count: usize = 0;
        let mut addr = None;

        // Skip functions and stuff just in case
        for (ind, b) in self.data.iter().enumerate().skip(0x72B860) {
            if *b == 0xFF {
                count += 1;
            } else if count >= size + 4 {
                addr = Some(ind - count + 2);
                break;
            } else {
                count = 0;
            }
        }

        addr
    }

    /// Override the current ROM type with a new type
    pub fn set_type(&mut self, version: Version) {
        let (sym_map, addr_size_map) = parse_sym(SYM_FILES[version as usize]);
        self.sym_map = sym_map;
        self.addr_size_map = addr_size_map;
        self.version = version;
    }

    /// Get the address and length data for a symbol
    /// entry by name. Names can be found in the source.
    pub fn get_sym_data(&self, k: &'static str) -> Option<&SymData> {
        self.sym_map.get(k)
    }

    /// Get the length data for a symbol by address.
    /// Does not round to nearest address.
    pub fn get_size_from_addr(&self, k: &usize) -> Option<&usize> {
        self.addr_size_map.get(k)
    }

    pub fn get_mon_offset_addr(
        &self,
        sym: &'static str,
        mon_id: u16,
        species_only: bool,
    ) -> Result<(usize, usize), Error> {
        let sym = self
            .get_sym_data(sym)
            .expect(&format!("No sym {} available", sym));
        let per_mon = sym.len
            / if species_only {
                NUM_MON_SPECIES
            } else {
                NUM_MON_SLOTS
            } as usize;
        let mon_offset = per_mon * mon_id as usize;

        if mon_offset >= sym.len {
            return Err(Error::InvalidId);
        }

        Ok((sym.addr + mon_offset - SYM_ADDR_TO_FILE, per_mon))
    }

    /// Doesn't work with any unevenly sized entries
    fn get_mon_specific_even_sized(
        &self,
        table: &'static str,
        mon_id: u16,
        species_only: bool,
    ) -> Result<Addressed<&[u8]>, Error> {
        let (offset_addr, size) = self.get_mon_offset_addr(table, mon_id, species_only)?;
        Ok(Addressed::new(
            offset_addr,
            &self.data[offset_addr..offset_addr + size],
        ))
    }

    /// Doesn't work with any unevenly sized entries
    fn set_mon_specific_even_sized(
        &mut self,
        table: &'static str,
        mon_id: u16,
        species_only: bool,
        data: &[u8],
    ) -> Result<(), Error> {
        let (offset_addr, size) = self.get_mon_offset_addr(table, mon_id, species_only)?;

        if data.len() > size {
            return Err(Error::TooMuchData);
        }

        self.overwrite(Addressed::new(offset_addr, data));
        Ok(())
    }

    fn get_sized_ptr_data(
        &self,
        table: &'static str,
        mon_id: u16,
        species_only: bool,
    ) -> Result<Addressed<&[u8]>, Error> {
        let (offset_addr, _) = self.get_mon_offset_addr(table, mon_id, species_only)?;

        // Ignore per_mon size and do 4 because we only want the ptr
        let ptr = u32::from_le_bytes(
            (&self.data[offset_addr..offset_addr + 4])
                .try_into()
                .unwrap(),
        ) as usize
            - SYM_ADDR_TO_FILE;
        let size = self
            .get_size_from_addr(&(ptr + SYM_ADDR_TO_FILE))
            .expect("Unsized front pic data");

        Ok(Addressed::new(ptr, &self.data[ptr..ptr + size]))
    }

    fn set_ptr_data(
        &mut self,
        table: &'static str,
        mon_id: u16,
        data: &[u8],
        species_only: bool,
    ) -> Result<(), Error> {
        let (offset_addr, _) = self.get_mon_offset_addr(table, mon_id, species_only)?;

        // Ignore per_mon size and do 4 because we only want the ptr
        let ptr = u32::from_le_bytes(
            (&self.data[offset_addr..offset_addr + 4])
                .try_into()
                .unwrap(),
        ) as usize
            - SYM_ADDR_TO_FILE;
        let size = self
            .get_size_from_addr(&(ptr + SYM_ADDR_TO_FILE))
            .expect("Unsized front pic data");

        if size < &data.len() {
            return Err(Error::TooMuchData);
        }

        self.overwrite(Addressed::new(ptr, data));
        Ok(())
    }

    //===================
    // Front Pic
    //===================

    pub fn get_front_pic_coords(&self, mon_id: u16) -> Result<Addressed<&[u8]>, Error> {
        // Size is 2 technically, dunno what the other 2 are
        self.get_mon_specific_even_sized("gMonFrontPicCoords", mon_id, false)
    }

    pub fn set_front_pic_coords(&mut self, mon_id: u16, data: &[u8]) -> Result<(), Error> {
        self.set_mon_specific_even_sized("gMonFrontPicCoords", mon_id, false, data)
    }

    /// Gets the data using the table pointers. Note
    /// that if you overwrite entries in that table, you
    /// will also receive that data here. For example if you
    /// replace the entire table with pointers to Magikarp's
    /// data, that's all you will receive from any mon_id
    /// here and old data won't be found.
    pub fn get_front_pic_data(&self, mon_id: u16) -> Result<Addressed<&[u8]>, Error> {
        self.get_sized_ptr_data("gMonFrontPicTable", mon_id, false)
    }

    pub fn set_front_pic_data(&mut self, mon_id: u16, data: &[u8]) -> Result<(), Error> {
        self.set_ptr_data("gMonFrontPicTable", mon_id, data, false)
    }

    /// The returned data is a little endian u32
    pub fn get_front_pic_ptr(&self, mon_id: u16) -> Result<Addressed<&[u8]>, Error> {
        self.get_mon_specific_even_sized("gMonFrontPicTable", mon_id, false)
    }

    pub fn set_front_pic_ptr(&mut self, mon_id: u16, data: &[u8]) -> Result<(), Error> {
        self.set_mon_specific_even_sized("gMonFrontPicTable", mon_id, false, data)
    }

    //===================
    // Icon
    //===================

    /// Gets the data using the table pointers. Note
    /// that if you overwrite entries in that table, you
    /// will also receive that data here. For example if you
    /// replace the entire table with pointers to Magikarp's
    /// data, that's all you will receive from any mon_id
    /// here and old data won't be found.
    pub fn get_icon_data(&self, mon_id: u16) -> Result<Addressed<&[u8]>, Error> {
        self.get_sized_ptr_data("gMonIconTable", mon_id, false)
    }

    pub fn set_icon_data(&mut self, mon_id: u16, data: &[u8]) -> Result<(), Error> {
        self.set_ptr_data("gMonIconTable", mon_id, data, false)
    }

    /// The returned data is a little endian u32
    pub fn get_icon_ptr(&self, mon_id: u16) -> Result<Addressed<&[u8]>, Error> {
        self.get_mon_specific_even_sized("gMonIconTable", mon_id, false)
    }

    pub fn set_icon_ptr(&mut self, mon_id: u16, data: &[u8]) -> Result<(), Error> {
        self.set_mon_specific_even_sized("gMonIconTable", mon_id, false, data)
    }

    //===================
    // Back Pic
    //===================

    pub fn get_back_pic_coords(&self, mon_id: u16) -> Result<Addressed<&[u8]>, Error> {
        self.get_mon_specific_even_sized("gMonBackPicCoords", mon_id, false)
    }

    pub fn set_back_pic_coords(&mut self, mon_id: u16, data: &[u8]) -> Result<(), Error> {
        self.set_mon_specific_even_sized("gMonBackPicCoords", mon_id, false, data)
    }

    /// Gets the data using the table pointers. Note
    /// that if you overwrite entries in that table, you
    /// will also receive that data here. For example if you
    /// replace the entire table with pointers to Magikarp's
    /// data, that's all you will receive from any mon_id
    /// here and old data won't be found.
    pub fn get_back_pic_data(&self, mon_id: u16) -> Result<Addressed<&[u8]>, Error> {
        self.get_sized_ptr_data("gMonBackPicTable", mon_id, false)
    }

    pub fn set_back_pic_data(&mut self, mon_id: u16, data: &[u8]) -> Result<(), Error> {
        self.set_ptr_data("gMonBackPicTable", mon_id, data, false)
    }

    /// The returned data is a little endian u32
    pub fn get_back_pic_ptr(&self, mon_id: u16) -> Result<Addressed<&[u8]>, Error> {
        self.get_mon_specific_even_sized("gMonBackPicTable", mon_id, false)
    }

    pub fn set_back_pic_ptr(&mut self, mon_id: u16, data: &[u8]) -> Result<(), Error> {
        self.set_mon_specific_even_sized("gMonBackPicTable", mon_id, false, data)
    }

    //===================
    // Palette
    //===================

    /// The returned data is a little endian u32
    pub fn get_palette_ptr(&self, mon_id: u16) -> Result<Addressed<&[u8]>, Error> {
        self.get_mon_specific_even_sized("gMonPaletteTable", mon_id, false)
    }

    pub fn set_palette_ptr(&mut self, mon_id: u16, data: &[u8]) -> Result<(), Error> {
        self.set_mon_specific_even_sized("gMonPaletteTable", mon_id, false, data)
    }

    /// Gets the data using the table pointers. Note
    /// that if you overwrite entries in that table, you
    /// will also receive that data here. For example if you
    /// replace the entire table with pointers to Magikarp's
    /// data, that's all you will receive from any mon_id
    /// here and old data won't be found.
    pub fn get_palette_data(&self, mon_id: u16) -> Result<Addressed<&[u8]>, Error> {
        self.get_sized_ptr_data("gMonPaletteTable", mon_id, false)
    }

    pub fn set_palette_data(&mut self, mon_id: u16, data: &[u8]) -> Result<(), Error> {
        self.set_ptr_data("gMonPaletteTable", mon_id, data, false)
    }

    //===================
    // Shiny Palette
    //===================

    /// The returned data is a little endian u32
    pub fn get_shiny_palette_ptr(&self, mon_id: u16) -> Result<Addressed<&[u8]>, Error> {
        self.get_mon_specific_even_sized("gMonShinyPaletteTable", mon_id, false)
    }

    pub fn set_shiny_palette_ptr(&mut self, mon_id: u16, data: &[u8]) -> Result<(), Error> {
        self.set_mon_specific_even_sized("gMonShinyPaletteTable", mon_id, false, data)
    }

    /// Gets the data using the table pointers. Note
    /// that if you overwrite entries in that table, you
    /// will also receive that data here. For example if you
    /// replace the entire table with pointers to Magikarp's
    /// data, that's all you will receive from any mon_id
    /// here and old data won't be found.
    pub fn get_shiny_palette_data(&self, mon_id: u16) -> Result<Addressed<&[u8]>, Error> {
        self.get_sized_ptr_data("gMonShinyPaletteTable", mon_id, false)
    }

    pub fn set_shiny_palette_data(&mut self, mon_id: u16, data: &[u8]) -> Result<(), Error> {
        self.set_ptr_data("gMonShinyPaletteTable", mon_id, data, false)
    }

    //===================
    // Base Stats
    //===================

    pub fn get_base_stats(&self, mon_id: u16) -> Result<Addressed<BaseStats>, Error> {
        let data =
            self.get_mon_specific_even_sized("gBaseStats", convert_to_species(mon_id), true)?;
        // TODO flesh out this error to be less vague
        data.try_into().map_err(|_| Error::InvalidData)
    }

    pub fn set_base_stats(&mut self, mon_id: u16, data: BaseStats) -> Result<(), Error> {
        // TODO flesh out this error to be less vague
        let data = data.to_bytes();
        self.set_mon_specific_even_sized("gBaseStats", convert_to_species(mon_id), true, &data)
    }

    //===================
    // Learnsets
    //===================

    /// The returned data is a little endian u32
    pub fn get_learn_set_ptr(&self, mon_id: u16) -> Result<Addressed<&[u8]>, Error> {
        self.get_mon_specific_even_sized("gLevelUpLearnsets", convert_to_species(mon_id), true)
    }

    pub fn set_learn_set_ptr(&mut self, mon_id: u16, data: &[u8]) -> Result<(), Error> {
        self.set_mon_specific_even_sized("gLevelUpLearnsets", convert_to_species(mon_id), true, data)
    }

    pub fn get_learn_set(&self, mon_id: u16) -> Result<Addressed<Vec<LearnSet>>, Error> {
        // This is a special pointer that can figure out its own size...
        let (offset_addr, _) = self.get_mon_offset_addr("gLevelUpLearnsets", convert_to_species(mon_id), true)?;

        // Ignore per_mon size and do 4 because we only want the ptr
        let ptr = u32::from_le_bytes(
            (&self.data[offset_addr..offset_addr + 4])
                .try_into()
                .unwrap(),
        ) as usize
            - SYM_ADDR_TO_FILE;

        let mut v: Vec<LearnSet> = Vec::with_capacity(128);
        // Arbitrary 512, hopefully never that much data to read
        for i in (ptr..ptr + 512).step_by(2) {
            if i - ptr > 250 {
                // We're reading too much here...
                return Err(Error::TooMuchData);
            }
            let u = (&self.data[i..i + 2]).get_u16_le();
            // Ends at 0xFFFF
            if u == 0xFFFF {
                break;
            }
            v.push(u.try_into().map_err(|_| Error::InvalidData)?);
        }

        Ok(Addressed::new(ptr, v))
    }

    /// Returns the new address
    pub fn set_learn_set(&mut self, mon_id: u16, sets: Vec<LearnSet>) -> Result<usize, Error> {
        // This is a special pointer that can figure out its own size...
        let (offset_addr, _) = self.get_mon_offset_addr("gLevelUpLearnsets", convert_to_species(mon_id), true)?;

        // Ignore per_mon size and do 4 because we only want the ptr
        let ptr = u32::from_le_bytes(
            (&self.data[offset_addr..offset_addr + 4])
                .try_into()
                .unwrap(),
        ) as usize
            - SYM_ADDR_TO_FILE;

        let usable_size = if let Some(size) = self
            .get_size_from_addr(&(ptr + SYM_ADDR_TO_FILE))
            .map(|v| *v)
        {
            size
        } else {
            let mut size = 0;
            let mut last = 0;
            // Arbitrary 512, hopefully never that much data to read
            for i in (ptr..ptr + 512).step_by(2) {
                if i - ptr > 250 {
                    // We're reading too much here...
                    return Err(Error::TooMuchData);
                }
                let u = (&self.data[i..i + 2]).get_u16_le();
                // Ends at 0xFFFF if no known size
                if u != 0xFFFF && last == 0xFFFF {
                    size = i - 2;
                    break;
                }
                last = u;
            }
            size
        };

        let required_size = sets.len() * 2;
        let sets_bytes = sets
            .iter()
            .map(|v| v.to_bytes())
            .flatten()
            .collect::<Vec<_>>();

        let addr = if required_size > usable_size {
            // relocate
            let new_addr = self
                .find_free_memory(required_size)
                .ok_or(Error::NoFreeSpace)?;
            let addr_bytes = ((new_addr + SYM_ADDR_TO_FILE) as u32).to_le_bytes();
            self.overwrite(Addressed::new(offset_addr, &addr_bytes));
            new_addr
        } else {
            // overwrite
            ptr
        };

        // Clear old data
        self.overwrite(Addressed::new(ptr, vec![0xFF; usable_size].as_slice()));

        self.overwrite(Addressed::new(addr, sets_bytes.as_slice()));

        Ok(addr)
    }

    //===================
    // Trainer
    //===================

    pub fn get_trainer(&self, id: u16) -> Result<Trainer, Error> {
        let table = self.get_sym_data("gTrainers").expect("No trainers table");
        // 40 each
        if id >= (table.len / 40) as u16 {
            return Err(Error::InvalidId);
        }

        let file_addr = table.addr - SYM_ADDR_TO_FILE;

        let offset = id as usize * 40;
        Ok(Trainer::try_from(&self.data[file_addr + offset..file_addr + offset + 40], &self.data).map_err(|_| Error::InvalidData)?)
    }

    /// This modifies the trainer if relocating the party's memory address
    /// is necessary.
    pub fn set_trainer(&mut self, id: u16, trainer: &mut Trainer) -> Result<(), Error> {
        let table = self.get_sym_data("gTrainers").expect("No trainers table");
        // 40 each
        if id >= (table.len / 40) as u16 {
            return Err(Error::InvalidId);
        }

        let file_addr = table.addr - SYM_ADDR_TO_FILE;

        let offset = id as usize * 40;

        let party_len = trainer.party_to_bytes().len();

        if trainer.prev_party_bytes < party_len {
            let wipe = vec![0xFF; trainer.prev_party_bytes];
            self.overwrite(Addressed::new(trainer.party.addr, wipe.as_slice()));
            trainer.party.addr = self.find_free_memory(party_len).ok_or(Error::NoFreeSpace)?;
        };

        trainer.prev_party_bytes = party_len;

        let (trainer_bytes, party) = trainer.to_bytes();

        self.overwrite(Addressed::new(file_addr + offset, &trainer_bytes));
        self.overwrite(Addressed::new(party.addr, &party.inner));

        Ok(())
    }

    //===================
    // Name
    //===================

    pub fn get_mon_name(&self, mon_id: u16) -> Result<Addressed<MonString>, Error> {
        let (addr, data) = self.get_mon_specific_even_sized("gSpeciesNames", convert_to_species(mon_id), true)?.unwrap();
        Ok(Addressed::new(addr, MonString::new(data)))
    }

    pub fn set_mon_name(&mut self, mon_id: u16, data: MonString) -> Result<(), Error> {
        self.set_mon_specific_even_sized("gSpeciesNames", convert_to_species(mon_id), true, data.as_slice())
    }

    //===================
    // Evos
    //===================

    pub fn get_evolutions(&self, mon_id: u16) -> Result<Addressed<[Evolution; 5]>, Error> {
        let (addr, data) = self.get_mon_specific_even_sized("gEvolutionTable", convert_to_species(mon_id), true)?.unwrap();
        let mut evos = Vec::with_capacity(5);

        for i in 0..5 {
            let offset = 8 * i;
            evos.push(Evolution::try_from(&data[offset..offset + 8]).map_err(|_| Error::InvalidData)?);
        }

        let mut fin: [Evolution; 5] = [Evolution::default(); 5];
        fin.copy_from_slice(&evos);

        Ok(Addressed::new(addr, fin))
    }

    pub fn set_evolutions(&mut self, mon_id: u16, data: [Evolution; 5]) -> Result<(), Error> {
        let evos: Vec<u8> = data.map(|v| v.to_bytes()).iter().flatten().map(|v| *v).collect();
        self.set_mon_specific_even_sized("gEvolutionTable", convert_to_species(mon_id), true, &evos)
    }

    //===================
    // Misc
    //===================

    /// 0 = bulbasaur, 1 = squirtle, 2 = charmander
    pub fn get_starter_mon(&self, starter: usize) -> Result<Addressed<Mon>, Error> {
        let scripts = ["PalletTown_ProfessorOaksLab_EventScript_BulbasaurBall", "PalletTown_ProfessorOaksLab_EventScript_SquirtleBall", "PalletTown_ProfessorOaksLab_EventScript_CharmanderBall"];

        if starter >= scripts.len() {
            return Err(Error::InvalidId);
        }

        let sym = self.get_sym_data(scripts[starter]).expect(&format!("No sym {}", scripts[starter]));
        let offset = sym.addr + 10 - SYM_ADDR_TO_FILE;
        let mon = Mon::from_u16((&self.data[offset..offset+2]).get_u16_le()).unwrap_or(Mon::None);

        Ok(Addressed::new(offset, mon))
    }

    /// 0 = bulbasaur, 1 = squirtle, 2 = charmander
    pub fn set_starter_mon(&mut self, starter: usize, to: Mon) -> Result<(), Error> {
        let scripts = ["PalletTown_ProfessorOaksLab_EventScript_BulbasaurBall", "PalletTown_ProfessorOaksLab_EventScript_SquirtleBall", "PalletTown_ProfessorOaksLab_EventScript_CharmanderBall"];

        if starter >= scripts.len() {
            return Err(Error::InvalidId);
        }

        // I don't know what this is to be honest but maybe it's important? Nobody else seems to change it
        let sym = self.get_sym_data("sStarterSpecies").expect("No sym sStarterSpecies");
        let offset = sym.addr + 2 * starter - SYM_ADDR_TO_FILE;
        let slicable = (to as u16).to_le_bytes();
        let new = Addressed::new(offset, slicable.as_slice());
        self.overwrite(new);

        let sym = self.get_sym_data(scripts[starter]).expect(&format!("No sym {}", scripts[starter]));
        let offset = sym.addr + 10 - SYM_ADDR_TO_FILE;
        let new = Addressed::new(offset, slicable.as_slice());
        self.overwrite(new);

        Ok(())
    }

    //===================
    // Moves
    //===================

    pub fn get_hm_moves(&self) -> Result<Addressed<[Move; NUM_HIDDEN_MACHINES]>, Error> {
        // TODO: This could get confused if sHMMoves mismatches, there's 2 isHmMove functions
        let sym = self.get_sym_data("sTMHMMoves").expect(&format!("No sym sTMHMMoves"));
        let offset = sym.addr + (NUM_TECHNICAL_MACHINES * 2) - SYM_ADDR_TO_FILE;
        let mut moves = [Move::None; NUM_HIDDEN_MACHINES];

        for hm in 0..NUM_HIDDEN_MACHINES {
            let hm_offset = offset + hm * 2;
            moves[hm] = Move::from_u16((&self.data[hm_offset..hm_offset+2]).get_u16_le()).unwrap_or(Move::None);
        }

        Ok(Addressed::new(offset, moves))
    }

    pub fn set_hm_move(&mut self, index: usize, to: Move) -> Result<(), Error> {
        if index >= NUM_HIDDEN_MACHINES {
            return Err(Error::InvalidId)
        }
        let sym = self.get_sym_data("sTMHMMoves").expect(&format!("No sym sTMHMMoves"));
        let offset = sym.addr + (NUM_TECHNICAL_MACHINES * 2) - SYM_ADDR_TO_FILE;

        let slicable = (to as u16).to_le_bytes();
        let new = Addressed::new(offset + index * 2, slicable.as_slice());

        self.overwrite(new);

        let sym_hm = self.get_sym_data("sHMMoves").expect(&format!("No sym sHMMoves"));
        let offset = sym_hm.addr - SYM_ADDR_TO_FILE;
        let new = Addressed::new(offset + index * 2, slicable.as_slice());

        self.overwrite(new);

        // If there's a dupe field we overwrite that as well (why is it here?)
        if let Some(sym_dupe) = self.get_sym_data("sTMHMMoves_Duplicate") {
            let offset = sym_dupe.addr + (NUM_TECHNICAL_MACHINES * 2) - SYM_ADDR_TO_FILE;

            let new = Addressed::new(offset + index * 2, slicable.as_slice());

            self.overwrite(new);
        }
        Ok(())
    }

    //===================
    // Flash
    //===================

    pub fn get_max_flash_level(&self) -> Result<Addressed<u32>, Error> {
        let sym = self.get_sym_data("gMaxFlashLevel").expect(&format!("No sym gMaxFlashLevel"));

        let addr = sym.addr - SYM_ADDR_TO_FILE;

        Ok(Addressed::new(sym.addr, (&self.data[addr..addr+sym.len]).get_u32_le()))
    }

    pub fn set_max_flash_level(&mut self, max: u32) -> Result<(), Error> {
        let sym = self.get_sym_data("gMaxFlashLevel").expect(&format!("No sym gMaxFlashLevel"));
        let new = max.to_le_bytes();
        let addr = sym.addr - SYM_ADDR_TO_FILE;
        self.overwrite(Addressed::new(addr, &new));

        Ok(())
    }
}
