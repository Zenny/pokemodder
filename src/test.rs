use crate::*;

#[test]
fn base_stats() {
    let modder = Modder::open("clean_rom_fr1_0.gba", None).expect("no rom");
    let stats = modder
        .get_base_stats(Mon::Bulbasaur as u16)
        .expect("bad stats");
    //println!("{:?}", stats.inner);
    assert_eq!(stats.inner.body_color, BodyColor::Green);
    assert_eq!(stats.inner.ability1, Ability::Overgrow);
    assert_eq!(stats.inner.ability2, Ability::None);
    assert_eq!(stats.inner.type1, Type::Grass);
    assert_eq!(stats.inner.type2, Type::Poison);
    assert_eq!(stats.inner.item1, Item::None);
    assert_eq!(stats.inner.item2, Item::None);
    assert_eq!(stats.inner.base_hp, 45);
    assert_eq!(stats.inner.base_atk, 49);
    assert_eq!(stats.inner.base_def, 49);
    assert_eq!(stats.inner.base_spd, 45);
    assert_eq!(stats.inner.safari_zone_flee_rate, 0);
    assert_eq!(stats.inner.gender_ratio, 31);
    assert_eq!(stats.inner.no_flip, false);
    assert_eq!(stats.inner.base_spatk, 65);
    assert_eq!(stats.inner.base_spdef, 65);
    assert_eq!(stats.inner.friendship, 70);
    assert_eq!(stats.inner.catch_rate, 45);
    assert_eq!(stats.inner.exp_yield, 64);
    assert_eq!(stats.inner.ev_yield_spatk, 1);
    assert_eq!(stats.inner.ev_yield_spdef, 0);
    assert_eq!(stats.inner.ev_yield_def, 0);
    assert_eq!(stats.inner.ev_yield_atk, 0);
    assert_eq!(stats.inner.ev_yield_spd, 0);
    assert_eq!(stats.inner.ev_yield_hp, 0);
}

#[test]
fn overwrite_learnset() {
    let mut modder = Modder::open("clean_rom_fr1_0.gba", None).expect("no rom");
    let (offset_addr, _) = modder
        .get_mon_offset_addr("gLevelUpLearnsets", 1, true)
        .expect("bad offset");
    let ptr = u32::from_le_bytes(
        (&modder.data[offset_addr..offset_addr + 4])
            .try_into()
            .unwrap(),
    ) as usize
        - SYM_ADDR_TO_FILE;
    let a = modder
        .set_learn_set(
            1,
            vec![LearnSet {
                level: 0,
                mv: Move::None,
            }],
        )
        .expect("Failed edit");
    assert_eq!(a, ptr);
    assert_eq!(
        &modder.data[ptr..ptr + 0x18],
        [
            0, 0, 0xFF, 0xFF, 0xFF, 0xFF, 0xFF, 0xFF, 0xFF, 0xFF, 0xFF, 0xFF, 0xFF, 0xFF, 0xFF,
            0xFF, 0xFF, 0xFF, 0xFF, 0xFF, 0xFF, 0xFF, 0xFF, 0xFF
        ]
    );
}

#[test]
fn huge_learnset() {
    let mut modder = Modder::open("clean_rom_fr1_0.gba", None).expect("no rom");
    let (offset_addr, _) = modder
        .get_mon_offset_addr("gLevelUpLearnsets", 1, true)
        .expect("bad offset");
    let ptr = u32::from_le_bytes(
        (&modder.data[offset_addr..offset_addr + 4])
            .try_into()
            .unwrap(),
    ) as usize
        - SYM_ADDR_TO_FILE;
    let a = modder
        .set_learn_set(
            1,
            vec![
                LearnSet {
                    level: 0,
                    mv: Move::None,
                };
                100
            ],
        )
        .expect("Failed edit");
    assert_ne!(a, ptr);
    let (offset_addr, _) = modder
        .get_mon_offset_addr("gLevelUpLearnsets", 1, true)
        .expect("bad offset");
    let ptr = u32::from_le_bytes(
        (&modder.data[offset_addr..offset_addr + 4])
            .try_into()
            .unwrap(),
    ) as usize
        - SYM_ADDR_TO_FILE;
    assert_eq!(a, ptr);
}

#[test]
fn trainer_bytes() {
    let modder = Modder::open("clean_rom_fr1_0.gba", None).expect("no rom");
    let trainer = modder.get_trainer(125).expect("bad trainer");
    assert_eq!(
        [0, 0x3B, 0x81, 0x54, 0xC2, 0xBB, 0xC6, 0xBF, 0xD3, 0xFF, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0x1, 0, 0, 0, 0x3, 0, 0, 0, 0xD8, 0xA6, 0x23, 0x8],
        trainer.to_bytes().0,
    );
}

#[test]
fn trainer() {
    let modder = Modder::open("clean_rom_fr1_0.gba", None).expect("no rom");
    let trainer = modder.get_trainer(740).expect("bad trainer");

    assert!(trainer.custom_moves);
    assert!(trainer.custom_held_item);
    assert_eq!(trainer.class, 90);
    assert_eq!(trainer.music, 0);
    assert_eq!(trainer.gender, Gender::M);
    assert_eq!(trainer.sprite, 125);
    assert_eq!(trainer.name.to_eng(), "TERRY".to_string());
    assert_eq!(trainer.items, [Item::FullRestore; 4]);
    assert!(!trainer.double_battle);
    assert_eq!(trainer.ai_flags, 7);
    assert_eq!(trainer.party_size, 6);
    assert_eq!(trainer.party.addr, 0x23E488);
    assert_eq!(trainer.party.inner, [Some(PartyMon { ev: 255, lvl: 72, species: Mon::Heracross, held_item: Some(Item::None), moves: Some([Move::Megahorn, Move::Earthquake, Move::Counter, Move::RockTomb]) }), Some(PartyMon { ev: 255, lvl: 73, species: Mon::Alakazam, held_item: Some(Item::None), moves: Some([Move::Psychic, Move::ShadowBall, Move::CalmMind, Move::Reflect]) }), Some(PartyMon { ev: 255, lvl: 72, species: Mon::Tyranitar, held_item: Some(Item::None), moves: Some([Move::Crunch, Move::Earthquake, Move::Thunderbolt, Move::AerialAce]) }), Some(PartyMon { ev: 255, lvl: 73, species: Mon::Gyarados, held_item: Some(Item::None), moves: Some([Move::HydroPump, Move::DragonDance, Move::Earthquake, Move::HyperBeam]) }), Some(PartyMon { ev: 255, lvl: 73, species: Mon::Arcanine, held_item: Some(Item::None), moves: Some([Move::ExtremeSpeed, Move::Overheat, Move::AerialAce, Move::IronTail]) }), Some(PartyMon { ev: 255, lvl: 75, species: Mon::Venusaur, held_item: Some(Item::SitrusBerry), moves: Some([Move::SolarBeam, Move::SludgeBomb, Move::Earthquake, Move::SunnyDay]) })])
}

#[test]
fn overwrite_party() {
    let mut modder = Modder::open("clean_rom_fr1_0.gba", None).expect("no rom");
    let mut trainer = modder.get_trainer(125).expect("bad trainer");
    let addr = trainer.party.addr;
    let size = trainer.prev_party_bytes;
    let mut party = trainer.get_party().clone();
    let f = party[0].as_mut().unwrap();
    f.moves = Some([Move::AerialAce, Move::Absorb, Move::Growth, Move::Acid]);
    trainer.custom_moves = true;
    trainer.set_party(party);

    modder.set_trainer(125, &mut trainer).expect("Couldn't set");

    assert_ne!(addr, trainer.party.addr);
    assert_ne!(size, trainer.prev_party_bytes);
}

#[test]
fn get_all_trainers() {
    let modder = Modder::open("clean_rom_fr1_0.gba", None).expect("no rom");
    for t in 0..NUM_TRAINERS {
        let trainer = modder.get_trainer(t).unwrap();
        let _ = trainer.get_party().clone();
    }
}

#[test]
fn mon_name() {
    let mut modder = Modder::open("clean_rom_fr1_0.gba", None).expect("no rom");

    let (_, karp) = modder.get_mon_name(129).expect("bad mon").unwrap();
    assert_eq!("MAGIKARP", karp.to_eng());

    modder.set_mon_name(129, MonString::from_eng("Derp").expect("bad str")).expect("bad set");

    let (_, karp) = modder.get_mon_name(129).expect("bad mon").unwrap();
    assert_eq!("Derp", karp.to_eng());
}

#[test]
fn evolution() {
    let mut modder = Modder::open("clean_rom_fr1_0.gba", None).expect("no rom");

    let (_, mut evos) = modder.get_evolutions(1).expect("bad mon").unwrap();

    assert_eq!(evos[0].species, Mon::Ivysaur);

    evos[0] = Evolution {
        method: 4,
        param: 0x10,
        species: Mon::Arcanine,
    };

    modder.set_evolutions(1, evos).expect("bad evos");

    let (_, evos) = modder.get_evolutions(1).expect("bad mon").unwrap();

    assert_eq!(evos[0].species, Mon::Arcanine);
}

#[test]
fn starter() {
    let mut modder = Modder::open("clean_rom_fr1_0.gba", None).expect("no rom");
    modder.set_starter_mon(0, Mon::Abra).unwrap();
    modder.set_starter_mon(1, Mon::Magikarp).unwrap();
    modder.set_starter_mon(2, Mon::Arcanine).unwrap();
    let (_, starter1) = modder.get_starter_mon(0).unwrap().unwrap();
    let (_, starter2) = modder.get_starter_mon(1).unwrap().unwrap();
    let (_, starter3) = modder.get_starter_mon(2).unwrap().unwrap();
    assert_eq!(starter1, Mon::Abra);
    assert_eq!(starter2, Mon::Magikarp);
    assert_eq!(starter3, Mon::Arcanine);
}

#[test]
fn hms() {
    let mut modder = Modder::open("clean_rom_fr1_0.gba", None).expect("no rom");

    for i in 0..NUM_HIDDEN_MACHINES {
        modder.set_hm_move(i, Move::Screech).expect("Failed set hm move");
    }

    let hms = modder.get_hm_moves().unwrap().inner;

    for hm in hms {
        assert_eq!(hm, Move::Screech);
    }
}

#[test]
fn flash() {
    let mut modder = Modder::open("clean_rom_fr1_0.gba", None).expect("no rom");
    modder.set_max_flash_level(1).expect("Can't set flash level");
    assert_eq!(modder.get_max_flash_level().unwrap().inner, 1);
}